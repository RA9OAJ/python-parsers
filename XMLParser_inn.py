import os
import xml.etree.ElementTree as et
from datetime import datetime
import sys


def check_and_lock(prefix):
    dir = os.path.dirname(sys.argv[0])
    lock_file = os.path.join(dir, f'{prefix}.lock')
    #  Check
    if os.path.exists(lock_file) and os.path.isfile(lock_file):
        return False

    # Lock
    fl = open(lock_file, 'w')
    fl.close()
    return True


def unlock(prefix):
    dir = os.path.dirname(sys.argv[0])
    lock_file = os.path.join(dir, f'{prefix}.lock')

    if os.path.exists(lock_file) and os.path.isfile(lock_file):
        os.remove(lock_file)
        return True

    return False


parse_errors = []


def get_paths():
    _cwd = os.getcwd()

    _paths = {
        'cwd': _cwd,
        'in': os.path.join(_cwd, 'in_inn'),
        'out': os.path.join(_cwd, 'out_inn')
    }

    if not os.path.isdir(_paths.get('in')):
        os.mkdir(_paths.get('in'))

    if not os.path.isdir(_paths.get('out')):
        os.mkdir(_paths.get('out'))
    return _paths


def parse_xml(file):
    try:
        root_node = et.parse(file).getroot()

        ns = {
            'self': 'http://www.roskazna.ru/eb/domain/finMonRcpt/formular'
        }

        tag_prefixes = ('', 'self:')

        find_tags = ('GUIDParentDoc', 'RcptDtTm')
        data = dict()

        for tag_prefix in tag_prefixes:
            for tag in root_node.findall(f'{tag_prefix}packetBody/{tag_prefix}rcptCollection/{tag_prefix}RcptCollection_ITEM/{tag_prefix}receipt', ns):
                guid = None

                for find_tag in find_tags:
                    tg = tag.find(f'{tag_prefix}{find_tag}', ns)

                    if tg is not None:
                        if find_tag == f'GUIDParentDoc':
                            guid = tg.text
                            data[guid] = dict()
                        else:
                            data[guid][find_tag] = datetime.fromisoformat(tg.text).strftime('%d.%m.%Y') if find_tag == 'RcptDtTm' else tg.text
                    else:
                        parse_errors.append(
                            f'В файле {os.path.basename(file)} отсутствуют необходимые для разборас труктуры.')
                        return dict()

                    for item in tag.findall(f'{tag_prefix}PstnRslt/{tag_prefix}PstnRslt_ITEM', ns):
                        field = item.find(f'{tag_prefix}FieldName', ns)

                        if field.text in ('P_INN', 'R_INN'):
                            inn_tag = item.find(f'{tag_prefix}FieldValue', ns)

                            data[guid][field.text] = inn_tag.text if inn_tag is not None else 'Отсутствет тэг'

        if not data:
            parse_errors.append(
                f'В файле {os.path.basename(file)} отсутствуют необходимые для разборас труктуры.')

        return data
    except:
        parse_errors.append('Файл %s: формат файла не XML' % os.path.basename(file))
        return dict()


def save_to_csv(data, folder):
    fl_name = '%s.csv' % datetime.now().strftime('%H-%M_%d-%m-%Y')
    fl_path = os.path.join(folder, fl_name)
    fl = open(fl_path, "wt")

    fl.write('"File";"Parent Doc GUID";"Date";"P_INN";"R_INN";\r\n')

    for fl_key in data.keys():
        fl_data = data.get(fl_key)

        for doc in fl_data.keys():
            str = '"{file}";"{guid}";"{date}";"{P_INN}";{R_INN};\r\n'.format(
                file=fl_key,
                guid=doc,
                date=fl_data.get(doc).get('RcptDtTm'),
                P_INN=fl_data.get(doc).get('P_INN', 'Отсутствет тэг'),
                R_INN=fl_data.get(doc).get('R_INN', 'Отсутствет тэг')
            )
            fl.write(str)

    fl.close()

    return fl_path


def archive(path_in):
    archive_dir = os.path.join(path_in, 'archive')

    if not os.path.exists(archive_dir):
        os.mkdir(archive_dir)

    cur_date_str = datetime.now().strftime('%H-%M_%d-%m-%Y')
    arch_full_path = os.path.join(archive_dir, cur_date_str)

    if not os.path.exists(arch_full_path):
        os.mkdir(arch_full_path)

    for cur in os.listdir(path_in):
        if os.path.isfile(os.path.join(path_in, cur)):
            os.rename(os.path.join(path_in, cur), os.path.join(arch_full_path, os.path.basename(cur)))

    return arch_full_path


def save_protocol(out_path, stringlist):
    proto_file = 'protocol_%s.txt' % datetime.now().strftime('%H-%M_%d-%m-%Y')
    proto_file = os.path.join(out_path, proto_file)

    fl = open(proto_file, 'wt')

    for cur in stringlist:
        fl.write(cur + '\r\n')

    fl.close()


if __name__ == '__main__':
    prefix = os.path.basename(sys.argv[0])

    if not check_and_lock(prefix):
        print('Обнаружен локфайл, данный процесс ничего делать не будет. Выход.')
        exit(0)

    paths = get_paths()

    cwd = paths.get('cwd')
    _in = paths.get('in')
    _out = paths.get('out')

    in_files = [f for f in os.listdir(_in) if os.path.isfile(os.path.join(_in, f))]
    protocol_strings = []

    if len(in_files):
        print('Всего файлов: %s' % len(in_files))
        protocol_strings.append('Обнаружено %s файлов\r\n' % len(in_files))

    dt = dict()

    for file in in_files:
        temp = parse_xml(os.path.join(_in, file))

        if temp:
            dt[file] = temp
        else:
            print('Ошибка при разборе файла %s' % file)

    if len(parse_errors):
        protocol_strings.append(f'Есть ошибки анализа ({len(parse_errors)} шт.):')
        protocol_strings += parse_errors
        protocol_strings.append('\r\n')

    if len(in_files):
        csv_str = 'Файл CSV выгружен по пути %s' % save_to_csv(dt, _out)
        print(csv_str)
        protocol_strings.append(csv_str)
        arch_str = 'Входящие файлы архивированы в директорию %s' % archive(_in)
        print(arch_str)
        protocol_strings.append(arch_str)
        protocol_strings.append('\r\nСписок обработанных файлов:')
        protocol_strings += in_files
        save_protocol(_out, protocol_strings)
    else:
        print('Файлы для анализа не обнаружены.')
        protocol_strings.append('Файлы для анализа не обнаружены.')

    unlock(prefix)
