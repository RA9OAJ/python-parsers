import os
import xml.etree.ElementTree as et
from datetime import datetime
import sys


def check_and_lock(prefix):
    dir = os.path.dirname(sys.argv[0])
    lock_file = os.path.join(dir, f'{prefix}.lock')
    #  Check
    if os.path.exists(lock_file) and os.path.isfile(lock_file):
        return False

    # Lock
    fl = open(lock_file, 'w')
    fl.close()
    return True


def unlock(prefix):
    dir = os.path.dirname(sys.argv[0])
    lock_file = os.path.join(dir, f'{prefix}.lock')

    if os.path.exists(lock_file) and os.path.isfile(lock_file):
        os.remove(lock_file)
        return True

    return False


parse_errors = []


def get_paths():
    _cwd = os.getcwd()

    _paths = {
        'cwd': _cwd,
        'in': os.path.join(_cwd, 'in'),
        'out': os.path.join(_cwd, 'out')
    }

    if not os.path.isdir(_paths.get('in')):
        os.mkdir(_paths.get('in'))

    if not os.path.isdir(_paths.get('out')):
        os.mkdir(_paths.get('out'))
    return _paths


def parse_xml(file):
    try:
        root_node = et.parse(file).getroot()

        ns = {
            'self': 'http://www.roskazna.ru/eb/domain/finMonRcpt/formular'
        }

        find_tags = ('GUIDParentDoc', 'RcptDtTm')
        data = dict()

        for tag in root_node.findall("self:packetBody/self:rcptCollection/self:RcptCollection_ITEM/self:receipt", ns):
            guid = None

            for find_tag in find_tags:
                tg = tag.find('self:%s' % find_tag, ns)

                if tg is not None:
                    if find_tag == 'GUIDParentDoc':
                        guid = tg.text
                        data[guid] = dict()
                    else:
                        data[guid][find_tag] = datetime.fromisoformat(tg.text).strftime('%d.%m.%Y') if find_tag == 'RcptDtTm' else tg.text

            result_tag = tag.find('self:PstnRslt', ns)

            for item in result_tag.findall('self:PstnRslt_ITEM', ns):
                control_name_tag = item.find('self:FieldName', ns)

                if control_name_tag is not None and control_name_tag.text == 'Name_reason':
                    mera_tag = item.find('self:FieldValue', ns)

                    if data[guid].get('reason_1') is None:
                        data[guid]['reason_1'] = mera_tag.text
                    else:
                        data[guid]['reason_2'] = mera_tag.text

            if 'reason_1' not in data[guid]:
                parse_errors.append('Файл %s: отсутствует тэг \'self:PstnRslt_ITEM/self:FieldName\'=\'Name_reason\''
                                    % os.path.basename(file))
                data[guid]['reason_1'] = 'Отсутствует тэг'
                data[guid]['reason_2'] = 'Отсутствует тэг'

        return data

    except:
        parse_errors.append('Файл %s: формат файла не XML' % os.path.basename(file))
        return None


def save_to_csv(data, folder):
    fl_name = '%s.csv' % datetime.now().strftime('%H-%M_%d-%m-%Y')
    fl_path = os.path.join(folder, fl_name)
    fl = open(fl_path, "wt")

    fl.write('"File";"Parent Doc GUID";"Date";"Reason 1";"Reason 2";\r\n')

    for fl_key in data.keys():
        fl_data = data.get(fl_key)

        for doc in fl_data.keys():
            str = '"{file}";"{guid}";"{date}";"{reason_1}";{reason_2};\r\n'.format(
                file=fl_key,
                guid=doc,
                date=fl_data.get(doc).get('RcptDtTm'),
                reason_1=fl_data.get(doc).get('reason_1'),
                reason_2=fl_data.get(doc).get('reason_2')
            )
            fl.write(str)

    fl.close()

    return fl_path


def archive(path_in):
    archive_dir = os.path.join(path_in, 'archive')

    if not os.path.exists(archive_dir):
        os.mkdir(archive_dir)

    cur_date_str = datetime.now().strftime('%H-%M_%d-%m-%Y')
    arch_full_path = os.path.join(archive_dir, cur_date_str)

    if not os.path.exists(arch_full_path):
        os.mkdir(arch_full_path)

    for cur in os.listdir(path_in):
        if os.path.isfile(os.path.join(path_in, cur)):
            os.rename(os.path.join(path_in, cur), os.path.join(arch_full_path, os.path.basename(cur)))

    return arch_full_path


def save_protocol(out_path, stringlist):
    proto_file = 'protocol_%s.txt' % datetime.now().strftime('%H-%M_%d-%m-%Y')
    proto_file = os.path.join(out_path, proto_file)

    fl = open(proto_file, 'wt')

    for cur in stringlist:
        fl.write(cur + '\r\n')

    fl.close()


if __name__ == '__main__':
    prefix = os.path.basename(sys.argv[0])

    if not check_and_lock(prefix):
        print('Обнаружен локфайл, данный процесс ничего делать не будет. Выход.')
        exit(0)

    paths = get_paths()

    cwd = paths.get('cwd')
    _in = paths.get('in')
    _out = paths.get('out')

    in_files = [f for f in os.listdir(_in) if os.path.isfile(os.path.join(_in, f))]
    protocol_strings = []

    if len(in_files):
        print('Всего файлов: %s' % len(in_files))
        protocol_strings.append('Обнаружено %s файлов\r\n' % len(in_files))

    dt = dict()

    for file in in_files:
        temp = parse_xml(os.path.join(_in, file))

        if temp is not None:
            dt[file] = temp
        else:
            print('Ошибка в формате файла %s' % file)
            protocol_strings.append('Ошибка в формате файла %s\r\n' % file)

    if len(parse_errors):
        protocol_strings.append('Есть ошибки анализа:')
        protocol_strings += parse_errors
        protocol_strings.append('\r\n')

    if len(in_files):
        csv_str = 'Файл CSV выгружен по пути %s' % save_to_csv(dt, _out)
        print(csv_str)
        protocol_strings.append(csv_str)
        arch_str = 'Входящие файлы архивированы в директорию %s' % archive(_in)
        print(arch_str)
        protocol_strings.append(arch_str)
        protocol_strings.append('\r\nСписок обработанных файлов:')
        protocol_strings += in_files
        save_protocol(_out, protocol_strings)
    else:
        print('Файлы для анализа не обнаружены.')
        protocol_strings.append('Файлы для анализа не обнаружены.')

    unlock(prefix)
