rem Путь до общей папки, где будут папки in/out
set cwd_path=C:\test2\

rem Путь до интерпритатора Python, если путь есть в переменной PATH, то можно просто указать python
set python_path=python

rem Путь до исполняемого скрипта
set script_path=C:\test\XMLParsr.py

rem Переход в CWD и вызов скрипта Python
cd %cwd_path%
%python_path% %script_path%
